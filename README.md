##### Name
mkopuslibrary - a recursive, multiprocess frontend to opusenc

##### Synopsis
`mkol.sh [--source] [--target] [option [argument]]...`

`mkol.sh [--source] [--target] [option [argument]]... --help`

`mkol.sh --version`

##### Description
mkopuslibrary converts trees of FLAC files into corresponding hierarchies of Opus files, while providing some additional related conveniences:
- copying MP3 files
- copying and/or embedding cover art files (with configurable size limits)
- listing directories missing artwork, or with artwork larger than set limits
- listing and optionally deleting orphaned target directories

##### Dependencies
- Bash >= v5.1: globstar (v4.0) / wait -n (v4.3) / wait -p (v5.1)
- coreutils: realpath / grealpath (or your realpath has same -e/-q behaviour)
- opusenc (optional)
- coreutils: comm / gcomm (optional)
- posix: cp (optional), less (optional), mkdir, wc, awk, rm (optional)

##### Documentation
- `-h | -H | --help`   Open full help text.
- `-hg | --general`    Open general help text.
- `-hs | --script`     Print script options.
- `-hm | --modes`      Print operating mode options.
- `-ho | --opus`       Print opus encode options
- `-ha | --art`        Print art options.
- `-hl | --log`        Print log options.

##### Usage
There are 4 operating modes: encoding opus, copying artwork, copying MP3s, and finding orphaned target directories. Each mode can be run alone or in addition to any other mode(s). See **Operating Mode Options** for more details.

Any arguments to command-line options must be quoted if they contain any non-alphanumeric characters (spaces, symbols, etc).

Help sections detailing command-line options will list the current setting of each option in brackets to the right their respective flags, including values for any options passed to the left of the `--help` / `-h` / `-H` flag.

Each configuration option may or may not apply to a given operation mode.

##### Configuration Files
Enable a user-config file by placing it in one of these locations:

    $HOME/.config/mkolrc
    $HOME/.config/mkopuslibrary/mkolrc
    $HOME/.mkolrc

An example config file, **mkolrc.example**, is provided in the mkopuslibrary package directory. The comments and settings are copied from the in-script default settings section.

A configuration file can include as many, or as few, of the options from **mkolrc.example** as you require.

At present configuration files are sourced directly. This is a potential security risk as any shell code within a config file will be executed. Every un-commented line **must only contain a variable definition** in the form of `variable_name="value"`.

##### Configuration Priority
The default configuration settings are found at the beginning of the script. These settings are superseded by those from a user-configuration file, when one is detected. Both script and user defaults are superseded by option flags on the command line.

Excluding `-d` / `--subdir`, options appearing right-most in an `mkol.sh` command replace and/or override any conflicting options appearing to their left.

When `-c` / `--config-file` is used to load a configuration file, options within that file will override any conflicting flags to the left of `-c` on the command line, and any conflicting options further to the right will override those in the file.

##### Artwork
Artwork files found in source directories can be copied to the corresponding directory in the Opus tree, and/or embedded in Opus outputs.

When either mode is enabled, each source directory is searched for files which case-insensitively match any combination of these names and extensions:

    folder, cover, front | jpg, jpeg, gif, webp, png

Size limits can be applied to either artwork mode. When limits are in use, each filename match in a directory is examined until one is found within the configured limit, or all filename matches have been exhausted.

In either mode, if no size limit is enabled, the first artwork filename match found in a source directory will be used as the file to embed/copy.

When embedding artwork is enabled, if a source artwork file is found, any existing artwork embedded in the source file will be omitted from the Opus output, and the detected artwork file will be embedded instead.

Unless `--discard-embedded-art` is used, source-embedded artwork will still be embedded in Opus outputs when `--embed-art` is not enabled, and regardless of any enabled size limits.

##### Logging
During applicable operations and with applicable options enabled, directories meeting the following criteria are logged:
  - source is missing artwork
  - artwork exists in source, but all candidates exceed copy size limit
  - artwork exists in source, but all candidates exceed embed size limit
  - target directory is orphaned (no matching source directory)
  - target directory could not be created

Each set of directories can be printed to stdout on completion, and/or saved to a file. See **Log Options** for more information. If printing and logging are both disabled, only counts of the directories matching each condition will be displayed - enabling at least one of these modes is recommended.
